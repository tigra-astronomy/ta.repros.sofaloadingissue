﻿// This file is part of the TA.Repros.SofaLoadingIssue project
// 
// Copyright © 2016-2017 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2017-04-10@12:19 by Tim Long

using System;
using ASCOM.Astrometry.Transform;

namespace TA.Repros.SofaLoadingIssue
    {
    internal class Program
        {
        private static void Main(string[] args)
            {
            var converter = new Transform();
            Console.ReadLine();
            }
        }
    }